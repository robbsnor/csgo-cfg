# csgo-cfg

## Install
1. Delete original CFG folder
2. Clone folder and rename it to "cfg"
3. Restore game files for original CFG's (optional)

## Location
..\Steam\steamapps\common\Counter-Strike Global Offensive\csgo\cfg

## Launch options
-novid -high +exec autoexec-

#### Optional launch options
-windowed -w 1280 -h 693


